﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Alert_Me.Models;
using System.Data.SqlClient;
using System.Data;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.AspNetCore.Http;
using static Microsoft.AspNetCore.Hosting.Internal.HostingApplication;
using Microsoft.AspNetCore.Hosting;

namespace Alert_Me.Controllers
{
    public class HomeController : Controller
    {
        string e;
        string p;
        string d;
        string s;

        static string DB = @"data source=10.0.0.16;initial catalog=SmartInsurance_AlertMe;user id=sa;password=Loaded10$";
        //static string CloudAccounting = @"data source=diazsoft.database.windows.net;initial catalog=SmartAccounting;user id=jmateo;password=Loaded10$";
        static string CloudAccounting = @"data source=10.0.0.16;initial catalog=SmartInsurance_AlertMe;user id=sa;password=Loaded10$";

        DataDB dbdata = new DataDB();
        string Id;
      
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        public IActionResult About()
        {
            return View();
        }

        public IActionResult Login()
        {
            return View();
        }

        public IActionResult SmartAccounting()
        {
            return View();
        }
        
        public IActionResult ViewEntidad()
        {
            List<Entidad> ente = new List<Entidad>();
            ente = dbdata.GetAllEnte().ToList();
            return PartialView(ente);
        }

        public IActionResult ViewFactura()
        {
            List<Servicios> servicios = new List<Servicios>();
            servicios = dbdata.GetAllServices().ToList();
            return PartialView(servicios);
        }

        public IActionResult ViewRRHH()
        {
            //List<Empleados> empleados = new List<Empleados>();
            //empleados = dbdata.GetAllEmpleados().ToList();

            var modelmix = new Tuple<List<Empleados>, List<Prestamos>, List<TSS>, List<IRS>, List<TipoDescuento>>(dbdata.GetAllEmpleados(), dbdata.GetAllAvanceSueldo(), dbdata.GetAllTSS(), dbdata.GetAllIRS(), dbdata.GetAllDescuento());

            List<Empleados> empleado = new List<Empleados>();
            empleado = dbdata.GetAllPuestos().ToList();
            List<SelectListItem> items = empleado.ConvertAll(d =>
            {
                return new SelectListItem()
                {
                  Text = d.Cargo.ToString(),
                  Value = d.IdCargo.ToString(),
                  Selected = false
                };
            });

            List<Empleados> dpt = new List<Empleados>();
            dpt = dbdata.GetAllDepartamento().ToList();
            List<SelectListItem> departamento = dpt.ConvertAll(d =>
            {
                return new SelectListItem()
                {
                    Text = d.Departamento.ToString(),
                    Value = d.Departamento.ToString(),
                    Selected = false
                };
            });

            List<Empleados> sp = new List<Empleados>();
            sp = dbdata.GetAllSupervisor().ToList();
            List<SelectListItem> supervisor = sp.ConvertAll(d =>
            {
                return new SelectListItem()
                {
                    Text = d.Supervisor.ToString(),
                    Value = d.Supervisor.ToString(),
                    Selected = false
                };
            });

            List<Empleados> epd = new List<Empleados>();
            epd = dbdata.GetAllEmpleadosPrestamo().ToList();
            List<SelectListItem> avanceempleado = epd.ConvertAll(d =>
            {
                return new SelectListItem()
                {
                    Text = d.Empleado.ToString(),
                    Value = d.TipoDoc.ToString(),
                    Selected = false
                };
            });

            List<TipoDescuento> desc = new List<TipoDescuento>();
            desc = dbdata.GetAllDescuento().ToList();
            List<SelectListItem> descuentoempleado = desc.ConvertAll(d =>
            {
                return new SelectListItem()
                {
                    Text = d.Tipo.ToString(),
                    Value = d.Tipo.ToString(),
                    Selected = false
                };
            });

            Getdatosrrh();
            ViewBag.empleados = e;
            ViewBag.puestos = p;
            ViewBag.dpt = d;
            ViewBag.sueldo = s;

            ViewBag.items = items;
            ViewBag.departmento = departamento;
            ViewBag.supervisor = supervisor;
            ViewBag.epd = avanceempleado;
            ViewBag.descuento = descuentoempleado;
            return View(modelmix);
        }

        public IActionResult ViewFormEntidad()
        {
            return View();
        }
       
        public void Getdatosrrh()
        {
            string sql = "Pr_Conteorrhh";
            using (SqlConnection con = new SqlConnection(CloudAccounting))
            {
                con.Open();
                using (SqlCommand comando = new SqlCommand(sql, con))
                {
                    comando.CommandType = CommandType.StoredProcedure;
                    using (SqlDataReader reader = comando.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            GeneralClass general = new GeneralClass()
                            {
                                empleados = reader.GetString(0),
                                puestos = reader.GetString(1),
                                dpt = reader.GetString(2),
                                Sueldo = reader.GetString(3)
                            };
                            e = general.empleados;
                            p = general.puestos;
                            d = general.dpt;
                            s = general.Sueldo;
                        }
                    }
                }
                con.Close();
            }
        }

        [HttpPost]
        public IActionResult Login(Account action)
        {
            string acceso = "Select Count(*) From Tbl_Usuarios Where UsuarioLog = @User And PWDCOMPARE(@Password, Clave) = 1";
            string store = "Pr_CrearUsuario";

            string usuario = Request.Form["usuario"];
            string nombre = Request.Form["nombre"];
            string apellido = Request.Form["apellido"];
            string email = Request.Form["email"];
            string psw = Request.Form["psw"];

            var name = Request.Form["Name"];
            action.ResulName = name;
            HttpContext.Session.SetString("Usuario", name);
            var password = Request.Form["Password"];
            action.ResulPassword = password;

            if (string.IsNullOrEmpty(nombre))
            {
                try
                {
                    using (SqlConnection connection = new SqlConnection(DB))
                    {
                        connection.Open();
                        SqlCommand command = new SqlCommand(acceso, connection);
                        command.Parameters.Add("@User", SqlDbType.VarChar, 100).Value = action.ResulName;
                        command.Parameters.Add("@Password", SqlDbType.VarChar, 100).Value = action.ResulPassword;
                        command.Connection = connection;
                        command.CommandText = acceso;
                        var result = command.ExecuteScalar();
                        Id = result.ToString();
                        connection.Close();
                    }
                }
                catch (Exception ex)
                {

                }
                return Id == "1" ? View("Index") : View("Login");
            }
            else
            {
                try
                {
                    using (SqlConnection connection = new SqlConnection(DB))
                    {
                        connection.Open();
                        SqlCommand command = new SqlCommand(store, connection)
                        {
                            CommandType = CommandType.StoredProcedure
                        };
                        command.Parameters.Add(new SqlParameter("@UsuarioLog", usuario));
                        command.Parameters.Add(new SqlParameter("@Nombre", nombre));
                        command.Parameters.Add(new SqlParameter("@Apellido", apellido));
                        command.Parameters.Add(new SqlParameter("@Clave", psw));
                        command.Parameters.Add(new SqlParameter("@Email", email));
                        command.Connection = connection;
                        command.CommandText = store;
                        command.ExecuteNonQuery();
                        connection.Close();
                    }
                }
                catch (Exception ex)
                {

                }
                return RedirectToAction("Login");
            }
        }

        [HttpPost]
        public IActionResult ViewRRHH(Puestos puesto)
        {
            string store = "Pr_RegistroCargos";
            string store1 = "Pr_RegistroDepartamento";
            string store2 = "Pr_RegistroSupervisor";
            string store3 = "Pr_RegistroEmpleados";
            string store4 = "Pr_RegistroAvanceEfectivo";
            string store5 = "Pr_RegistroDescuento";

            string cargo = Request.Form["DetPuesto"];
            string departamento = Request.Form["DetDepartamento"];
            string supervisor = Request.Form["DetSupervisor"];
            string supervisordpt = Request.Form["datosdpt"];

            string img = Request.Form["imgload"];
            string ruta = ViewBag.img;

            string Nombre = Request.Form["NombreEmpleado"];
            string Apellido = Request.Form["ApellidoEmpleado"];
            string TipoDoc = Request.Form["TipoDocEmpleado"];
            string NumDoc = Request.Form["NumDocEmpleado"];
            string Sexo = Request.Form["SexoEmpleado"];
            string CargoEmpleado = Request.Form["CargoEmpleado"];
            string DepartamentoEmpleado = Request.Form["DepartamentoEmpleado"];
            string SupervisorEmpleado = Request.Form["SupervisorEmpleado"];
            string Sueldo = Request.Form["SueldoEmpleado"];
            string FecIngreso = Request.Form["FecIngresoEmpleado"];
            string FecAniversario = Request.Form["FecAniversarioEmpleado"];
            string CuentaBancaria = Request.Form["CuentaBancariaEmpleado"];
            string TelContacto = Request.Form["TelContactoEmpleado"];
            string Email = Request.Form["EmailEmpleado"];
            string Correspondencia = Request.Form["CorrespondenciaEmpleado"];
            string EstadoPais = Request.Form["EstadoPaisEmpleado"];
            string Pais = Request.Form["PaisEmpleado"];

            string DropEmpleado = Request.Form["datosAvanceEmpleado"];
            string MontoAvance = Request.Form["MontoAvance"];
            string CuotasAvance = Request.Form["CuotasAvance"];
            string AvanceDesc = Request.Form["AvanceDesc"];

            string empleadodescuento = Request.Form["datosdescuentoempleado"];
            string datostipodescuento = Request.Form["datostipodescuento"];
            string MontoDescuento = Request.Form["MontoDescuento"];

            //RegistroCargo
            if (!string.IsNullOrEmpty(cargo))
            {
                try
                {
                    using (SqlConnection connection = new SqlConnection(CloudAccounting))
                    {
                        connection.Open();
                        SqlCommand command = new SqlCommand(store, connection)
                        {
                            CommandType = CommandType.StoredProcedure
                        };
                        command.Parameters.Add(new SqlParameter("@Usuario", HttpContext.Session.GetString("Usuario")));
                        command.Parameters.Add(new SqlParameter("@Cargo", cargo));
                        command.Connection = connection;
                        command.CommandText = store;
                        command.ExecuteNonQuery();
                        connection.Close();
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("IOException source: {0}", ex.Source);
                }
            }
            else if (!string.IsNullOrEmpty(departamento))
            //RegistroDepartamento
            {
                try
                {
                    using (SqlConnection connection = new SqlConnection(CloudAccounting))
                    {
                        connection.Open();
                        SqlCommand command = new SqlCommand(store1, connection)
                        {
                            CommandType = CommandType.StoredProcedure
                        };
                        command.Parameters.Add(new SqlParameter("@Usuario", HttpContext.Session.GetString("Usuario")));
                        command.Parameters.Add(new SqlParameter("@Departamento", departamento));
                        command.Connection = connection;
                        command.CommandText = store1;
                        command.ExecuteNonQuery();
                        connection.Close();
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("IOException source: {0}", ex.Source);
                }
            }
            else if (!string.IsNullOrEmpty(supervisor))
            //RegistroSupervisor
            {
                try
                {
                    using (SqlConnection connection = new SqlConnection(CloudAccounting))
                    {
                        connection.Open();
                        SqlCommand command = new SqlCommand(store2, connection)
                        {
                            CommandType = CommandType.StoredProcedure
                        };
                        command.Parameters.Add(new SqlParameter("@Usuario", HttpContext.Session.GetString("Usuario")));
                        command.Parameters.Add(new SqlParameter("@Supervisor", supervisor));
                        command.Parameters.Add(new SqlParameter("@Dpt", supervisordpt));
                        command.Connection = connection;
                        command.CommandText = store2;
                        command.ExecuteNonQuery();
                        connection.Close();
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("IOException source: {0}", ex.Source);
                }
            }
            else if (!string.IsNullOrEmpty(Nombre))
            {
                try
                {
                    using (SqlConnection connection = new SqlConnection(CloudAccounting))
                    {
                        connection.Open();
                        SqlCommand command = new SqlCommand(store3, connection)
                        {
                            CommandType = CommandType.StoredProcedure
                        };
                        command.Parameters.Add(new SqlParameter("@Nombre", Nombre));
                        command.Parameters.Add(new SqlParameter("@Apellido", Apellido));
                        command.Parameters.Add(new SqlParameter("@TipoDoc", TipoDoc));
                        command.Parameters.Add(new SqlParameter("@NumDoc", NumDoc));
                        command.Parameters.Add(new SqlParameter("@Sexo", Sexo));
                        command.Parameters.Add(new SqlParameter("@Cargo", CargoEmpleado));
                        command.Parameters.Add(new SqlParameter("@Dpt", DepartamentoEmpleado));
                        command.Parameters.Add(new SqlParameter("@Supervisor", SupervisorEmpleado));
                        command.Parameters.Add(new SqlParameter("@Sueldo", Sueldo));
                        command.Parameters.Add(new SqlParameter("@FecIngreso", FecIngreso));
                        command.Parameters.Add(new SqlParameter("@FecAniversario", FecAniversario));
                        command.Parameters.Add(new SqlParameter("@CuentaBancaria", CuentaBancaria));
                        command.Parameters.Add(new SqlParameter("@TelContacto", TelContacto));
                        command.Parameters.Add(new SqlParameter("@Email", Email));
                        command.Parameters.Add(new SqlParameter("@Corres", Correspondencia));
                        command.Parameters.Add(new SqlParameter("@EstadoPais", EstadoPais));
                        command.Parameters.Add(new SqlParameter("@Pais", Pais));
                        command.Parameters.Add(new SqlParameter("@Img", string.Empty));
                        command.Parameters.Add(new SqlParameter("@Usuario", HttpContext.Session.GetString("Usuario")));
                        command.Connection = connection;
                        command.CommandText = store3;
                        command.ExecuteNonQuery();
                        connection.Close();
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("IOException source: {0}", ex.Source);
                }
            }
            else if (!string.IsNullOrEmpty(DropEmpleado))
            {
                try
                {
                    using (SqlConnection connection = new SqlConnection(CloudAccounting))
                    {
                        connection.Open();
                        SqlCommand command = new SqlCommand(store4, connection)
                        {
                            CommandType = CommandType.StoredProcedure
                        };
                        command.Parameters.Add(new SqlParameter("@Usuario", HttpContext.Session.GetString("Usuario")));
                        command.Parameters.Add(new SqlParameter("@Empleado", DropEmpleado));
                        command.Parameters.Add(new SqlParameter("@Monto", MontoAvance));
                        command.Parameters.Add(new SqlParameter("@cuotas", CuotasAvance));
                        command.Parameters.Add(new SqlParameter("@Obs", AvanceDesc));
                        command.Connection = connection;
                        command.CommandText = store4;
                        command.ExecuteNonQuery();
                        connection.Close();
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("IOException source: {0}", ex.Source);
                }
            }
            else if (!string.IsNullOrEmpty(datostipodescuento))
            {
                try
                {
                    using (SqlConnection connection = new SqlConnection(CloudAccounting))
                    {
                        connection.Open();
                        SqlCommand command = new SqlCommand(store5, connection)
                        {
                            CommandType = CommandType.StoredProcedure
                        };
                        command.Parameters.Add(new SqlParameter("@Usuario", HttpContext.Session.GetString("Usuario")));
                        command.Parameters.Add(new SqlParameter("@IdTipo", datostipodescuento));
                        command.Parameters.Add(new SqlParameter("@Empleado", empleadodescuento));
                        command.Parameters.Add(new SqlParameter("@Monto", MontoDescuento));
                        command.Connection = connection;
                        command.CommandText = store5;
                        command.ExecuteNonQuery();
                        connection.Close();
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("IOException source: {0}", ex.Source);
                }
            }

            return RedirectToAction("ViewRRHH");
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
