﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Alert_Me.Models
{
    public class Entidad
    {
        public int IdEntidad { get; set; }

        public string Nombre { get; set; }

        public string Apellido { get; set; }

        public string RazonSocial { get; set; }

        public string NroTelefono { get; set; }

        public string TipoTelefono { get; set; }

        public string Nrotelefono1 { get; set; }

        public string Tipotelefono1 { get; set; }

        public string TipoEnte { get; set; }

        public string Sexo { get; set; }

        public string NroDoc { get; set; }

        public string TipoDoc { get; set; }

        public string TipoDomicilio { get; set; }

        public string Calle { get; set; }

        public string Sector { get; set; }

        public string Departamento { get; set; }

        public string Pais { get; set; }

        public string WebSite { get; set; }

        public string Email { get; set; }

        public string Empresa { get; set; }

        public string Cargo { get; set; }

        public string GiroNegocio { get; set; }

        public string Cortesia { get; set; }

        public string EstadoCivil { get; set; }

        public string FecAniversario { get; set; }

        public string Instagram { get; set; }

        public string FaceBook { get; set; }

        public string Estado { get; set; }

        public DateTime FecRegistro { get; set; }

        public string UsuarioReg { get; set; }

        public string Compania { get; set; }
    }
}
