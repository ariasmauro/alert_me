﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Alert_Me.Models
{
    public class Prestamos
    {
        //Campos solo para usar consulta de avance de sueldo
        public string IdAvance { get; set; }
        public string Colaborador { get; set; }
        public string MontoSolicitado { get; set; }
        public string MontoSaldo { get; set; }
        public string Cuotas { get; set; }
        public string CuotasRestante { get; set; }
    }
}
