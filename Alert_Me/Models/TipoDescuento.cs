﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Alert_Me.Models
{
    public class TipoDescuento
    {
        public string IdTipo { get; set; }
        public string Tipo { get; set; }
        public string Usuario { get; set; }
    }
}
