﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Alert_Me.Models
{
    public class IRS
    {
        public string Escala { get; set; }
        public string Desde { get; set; }
        public string Hasta { get; set; }
        public string Aporte { get; set; }
    }
}
