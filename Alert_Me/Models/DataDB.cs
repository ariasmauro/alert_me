﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace Alert_Me.Models
{
    public class DataDB
    {
        //string connectionString = "data source=diazsoft.database.windows.net;initial catalog=SmartAccounting;user id=jmateo;password=Loaded10$";
        string connectionString = @"data source=10.0.0.16;initial catalog=SmartInsurance_AlertMe;user id=sa;password=Loaded10$";
            
        public IEnumerable<Entidad> GetAllEnte()
        {
            List<Entidad> listaente = new List<Entidad>();
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand("Pr_ConsultaEntidad", con);
                cmd.CommandType = CommandType.StoredProcedure;

                con.Open();
                SqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    Entidad entidad = new Entidad
                    {
                        IdEntidad = Convert.ToInt32(rdr["IdEntidad"]),
                        RazonSocial = rdr["RazonSocial"].ToString(),
                        TipoEnte = rdr["TipoEnte"].ToString(),
                        Sexo = rdr["Sexo"].ToString(),
                        TipoDoc = rdr["TipoDoc"].ToString(),
                        NroDoc = rdr["NroDoc"].ToString(),
                        UsuarioReg = rdr["UsuarioReg"].ToString(),
                        NroTelefono = rdr["Telefono"].ToString()
                    };

                    listaente.Add(entidad);
                }
                con.Close();
            }
            return listaente;
        }

        public IEnumerable<Servicios> GetAllServices()
        {
            List<Servicios> listaservicios = new List<Servicios>();
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand("Pr_Servicios", con);
                cmd.CommandType = CommandType.StoredProcedure;

                con.Open();
                SqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    Servicios servicios = new Servicios
                    {
                        Id = Convert.ToInt32(rdr["Id"]),
                        Services = rdr["Servicios"].ToString()
                    };

                    listaservicios.Add(servicios);
                }
                con.Close();
            }
            return listaservicios;
        }

        public List<Empleados> GetAllEmpleados()
        {
            List<Empleados> listaempleados = new List<Empleados>();
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand("Pr_ConsultaEmpleados", con);
                cmd.CommandType = CommandType.StoredProcedure;

                con.Open();
                SqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    Empleados empleados = new Empleados
                    {
                        Empleado = rdr["Empleado"].ToString(),
                        TipoDoc = rdr["TipoDocumento"].ToString(),
                        NroDoc = rdr["Documento"].ToString(),
                        Dpt = rdr["Departamento"].ToString(),
                        Cargo = rdr["Cargo"].ToString(),
                        Sueldo = rdr["Sueldo"].ToString()
                    };

                    listaempleados.Add(empleados);
                }
                con.Close();
            }
            return listaempleados;
        }

        public IEnumerable<Empleados> GetAllPuestos()
        {
            List<Empleados> listapuestos = new List<Empleados>();
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand("Pr_ConsultaPuestos", con);
                cmd.CommandType = CommandType.StoredProcedure;

                con.Open();
                SqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    Empleados puesto = new Empleados
                    {
                        Cargo = rdr["Cargo"].ToString(),
                        IdCargo = rdr["Cargo"].ToString()
                    };

                    listapuestos.Add(puesto);
                }
                con.Close();
            }
            return listapuestos;
        }

        public IEnumerable<Empleados> GetAllDepartamento()
        {
            List<Empleados> listadepartamento = new List<Empleados>();
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand("Pr_ConsultaDepartamento", con);
                cmd.CommandType = CommandType.StoredProcedure;

                con.Open();
                SqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    Empleados departamento = new Empleados
                    {
                        Departamento = rdr["Departamento"].ToString(),
                        IdCargo = rdr["Departamento"].ToString()
                    };

                    listadepartamento.Add(departamento);
                }
                con.Close();
            }
            return listadepartamento;
        }

        public IEnumerable<Empleados> GetAllSupervisor()
        {
            List<Empleados> listasupervisor = new List<Empleados>();
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand("Pr_ConsultaSupervisor", con);
                cmd.CommandType = CommandType.StoredProcedure;

                con.Open();
                SqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    Empleados supervisor = new Empleados
                    {
                        Supervisor = rdr["Supervisor"].ToString(),
                        IdSupervisor = rdr["Supervisor"].ToString()
                    };

                    listasupervisor.Add(supervisor);
                }
                con.Close();
            }
            return listasupervisor;
        }

        public IEnumerable<Empleados> GetAllEmpleadosPrestamo()
        {
            List<Empleados> listaempleados = new List<Empleados>();
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand("Pr_ConsultaEmpleados", con);
                cmd.CommandType = CommandType.StoredProcedure;

                con.Open();
                SqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    Empleados empleados = new Empleados
                    {
                        Empleado = rdr["Empleado"].ToString(),
                        TipoDoc = rdr["Documento"].ToString()
                    };

                    listaempleados.Add(empleados);
                }
                con.Close();
            }
            return listaempleados;
        }

        public List<Prestamos> GetAllAvanceSueldo()
        {
            List<Prestamos> listaempleados = new List<Prestamos>();
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand("Pr_ConsultaDeuda", con);
                cmd.CommandType = CommandType.StoredProcedure;

                con.Open();
                SqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    Prestamos empleados = new Prestamos
                    {
                        IdAvance = rdr["IdAvance"].ToString(),
                        Colaborador = rdr["Colaborador"].ToString(),
                        MontoSolicitado = rdr["MontoSolicitado"].ToString(),
                        MontoSaldo = rdr["MontoSaldo"].ToString(),
                        Cuotas = rdr["Cuotas"].ToString(),
                        CuotasRestante = rdr["Cuota_Restante"].ToString()
                    };

                    listaempleados.Add(empleados);
                }
                con.Close();
            }
            return listaempleados;
        }

        public List<TSS> GetAllTSS()
        {
            List<TSS> listatss= new List<TSS>();
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand("Pr_TSS", con);
                cmd.CommandType = CommandType.StoredProcedure;

                con.Open();
                SqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    TSS tss = new TSS
                    {
                        Id = rdr["Id"].ToString(),
                        Concepto = rdr["Concepto"].ToString(),
                        Porciento = rdr["PorcientoAporte"].ToString(),
                    };

                    listatss.Add(tss);
                }
                con.Close();
            }
            return listatss;
        }

        public List<IRS> GetAllIRS()
        {
            List<IRS> listairs = new List<IRS>();
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand("Pr_ISR", con);
                cmd.CommandType = CommandType.StoredProcedure;

                con.Open();
                SqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    IRS irs = new IRS
                    {
                        Escala = rdr["Escala"].ToString(),
                        Desde = rdr["Desde"].ToString(),
                        Hasta = rdr["Hasta"].ToString(),
                        Aporte = rdr["Aporte"].ToString()
                    };

                    listairs.Add(irs);
                }
                con.Close();
            }
            return listairs;
        }

        public List<TipoDescuento> GetAllDescuento()
        {
            List<TipoDescuento> listadescuento = new List<TipoDescuento>();
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                SqlCommand cmd = new SqlCommand("Pr_TiposDescuento", con);
                cmd.CommandType = CommandType.StoredProcedure;

                con.Open();
                SqlDataReader rdr = cmd.ExecuteReader();

                while (rdr.Read())
                {
                    TipoDescuento descuento = new TipoDescuento
                    {
                        IdTipo = rdr["IdTipo"].ToString(),
                        Tipo = rdr["TipoDescuento"].ToString(),
                        Usuario = rdr["UsuarioReg"].ToString()
                    };

                    listadescuento.Add(descuento);
                }
                con.Close();
            }
            return listadescuento;
        }

        public void Getdatosrrh()
        {
            string sql = "Pr_Conteorrhh";
            using (SqlConnection con = new SqlConnection(connectionString))
            {
                con.Open();
                using (SqlCommand comando = new SqlCommand(sql, con))
                {
                    comando.CommandType = CommandType.StoredProcedure;
                    using (SqlDataReader reader = comando.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            GeneralClass general = new GeneralClass()
                            {
                                empleados = reader.GetString(0),
                                puestos = reader.GetString(1),
                                dpt = reader.GetString(2)
                            };
                        }
                    }
                }
                con.Close();
            }
        }
    }
}
