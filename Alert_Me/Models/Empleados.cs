﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Alert_Me.Models
{
    public class Empleados
    {
        public string Empleado { get; set; }

        public string TipoDoc { get; set; }

        public string NroDoc { get; set; }

        public string Dpt { get; set; }

        public string Cargo { get; set; }

        public string Sueldo { get; set; }

        //Campos solo para usar consulta de otra tabla
        public string Puesto { get; set; }
        public string IdCargo { get; set; }
        public string Departamento { get; set; }
        public string IdDepartamento { get; set; }
        public string Supervisor { get; set; }
        public string IdSupervisor { get; set; }
    }

    
}
