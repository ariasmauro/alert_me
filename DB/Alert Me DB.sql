Create DataBase SmartInsurance_AlertMe
Go
Use SmartInsurance_AlertMe
Go
Create Table Tbl_Usuarios
(
IdUsuario Int Not Null Identity(1,1), -- Id Usuario
UsuarioLog Varchar(100) Not Null, -- Nombre de Usuario
Nombre Varchar(100) Not Null, -- Nombre
Apellido Varchar(100) Not Null, -- Apellido
Clave Varbinary(Max) NOT NULL, -- Clave de Acceso
Email Varchar(100) Not Null, -- Correo Electronico
FecRegistro DateTime Not Null Default(Getdate()), -- Fecha de Registro
UsuarioReg Varchar(100) Not Null -- Usuario Registro
)
Go
Insert Into Tbl_Usuarios(UsuarioLog, Nombre, Apellido, Clave, Email, UsuarioReg) 
     Values ('jmateo','Josias Manuel','Mateo D�az',PWDENCRYPT('Loaded10$'),'jmateo@diazsoft.com.do','root')
Go
-- Select Count(*) From Tbl_Usuarios Where UsuarioLog = 'jmateo' And PWDCOMPARE('Loaded10$', Clave) = 1
Create Procedure Pr_CrearUsuario(@UsuarioLog Varchar(100), @Nombre Varchar(100), @Apellido Varchar(100), @Clave Varchar(50), @Email Varchar(100))
As
Begin
    Insert Into Tbl_Usuarios(UsuarioLog, Nombre, Apellido, Clave, Email, UsuarioReg) 
     Values (@UsuarioLog,@Nombre,@Apellido,PWDENCRYPT(@Clave),@Email,'WebClient')
End
Go
Create Table Tbl_Empleados
(
IdEmpleado Int Not Null Identity(1,1) Primary Key,
Nombre Varchar(50) Not Null,
Apellido Varchar(50) Not Null,
TipoDoc Varchar(25) Not Null,
NumDoc Varchar(35) Not Null,
Sexo Varchar(15) Not Null,
IdCargo Int Not Null,
IdDepartamento Int Not Null,
IdSupervisor Int Not Null,
Sueldo Decimal(16,2) Not Null,
FecIngreso DateTime Not Null,
FecAniversario Datetime Not Null,
CuentaBancaria Varchar(25) Not Null,
TelContacto Varchar(25) Not Null,
Email Varchar(50) Not Null,
Correspondencia Varchar(200) Not Null,
EstadoPais Varchar(50) Not Null,
Pais Varchar (50) Not Null,
Img Varchar(100) Null,
Estado Char(1) Not Null Default('A'),
FecRegistro DateTime Not Null Default(Getdate()),
UsuarioReg Varchar(50) Not Null
)
Go
Create Procedure Pr_RegistroEmpleados (@Nombre Varchar(50),@Apellido Varchar(50),@TipoDoc Varchar(25),@NumDoc Varchar(35),@Sexo Varchar(15),@Cargo Varchar(50),@Dpt Varchar(50),@Supervisor Varchar(50),
                                       @Sueldo Decimal(16,2),@FecIngreso DateTime,@FecAniversario DateTime,@CuentaBancaria Varchar(25),@TelContacto Varchar(25),@Email Varchar(50),@Corres Varchar(200),
									   @EstadoPais Varchar(50),@Pais Varchar(50),@Img Varchar(100),@Usuario Varchar(50))
As
Declare @IDCargo Int = (Select IdCargo From Tbl_Cargos Where Cargo = @Cargo)
Declare @IdDpt Int = (Select IdDepartamento From Tbl_Departamento Where Departamento = @Dpt)
Declare @IdSup Int = (Select IdSupervisor From Tbl_Supervisor Where Supervisor = @Supervisor)
Begin
   Insert Into Tbl_Empleados(Nombre,Apellido,TipoDoc,NumDoc,Sexo,IdCargo,IdDepartamento,IdSupervisor,Sueldo,FecIngreso,FecAniversario,CuentaBancaria,TelContacto,Email,Correspondencia,EstadoPais,
                             Pais,Img,UsuarioReg)
   Values(@Nombre,@Apellido,@TipoDoc,@NumDoc,@Sexo,@IdCargo,@IdDpt,@IdSup,@Sueldo,@FecIngreso,@FecAniversario,@CuentaBancaria,@TelContacto,@Email,@Corres,@EstadoPais,
          @Pais,@Img,@Usuario)
End
Go
Create Procedure Pr_ConsultaEmpleados
As
Select Concat(Nombre,' ',Apellido)Empleado, TipoDoc [TipoDocumento], NumDoc [Documento], D.Departamento Departamento, C.Cargo Cargo, CONVERT(varchar, convert(money, Sueldo), 1)Sueldo
From Tbl_Empleados E
     Join Tbl_Departamento D on E.IdDepartamento = D.IdDepartamento
	 Join Tbl_Cargos C On E.IdCargo = C.IdCargo
Go
Create Table Tbl_Cargos
(
IdCargo Int Not Null Identity(1,1) Primary key,
Cargo Varchar(50) Not Null,
Estado Char(1) Not Null Default('A'),
FecRegistro DateTime Not Null Default(Getdate()),
UsuarioReg Varchar(50) Not Null
)
Go
Create Procedure Pr_RegistroCargos (@Cargo Varchar(50),@Usuario Varchar(50))
As
Begin
      Insert Into Tbl_Cargos(Cargo,UsuarioReg)
	   Values (@Cargo,@Usuario)
	  RaisError('Cargo registrado correctamente',16,1)
End
Go
Create Procedure Pr_ConsultaPuestos
As
Select IdCargo, Cargo From Tbl_Cargos Where Estado = 'A'
Go
Create Table Tbl_Departamento
(
IdDepartamento Int Not Null Identity(1,1) Primary key,
Departamento Varchar(50) Not Null,
Estado Char(1) Not Null Default('A'),
FecRegistro DateTime Not Null Default(Getdate()),
UsuarioReg Varchar(50) Not Null
)
Go
Create Procedure Pr_ConsultaDepartamento
As
Select IdDepartamento, Departamento From Tbl_Departamento Where Estado = 'A'
Go
Create Procedure Pr_RegistroDepartamento (@Departamento Varchar(50),@Usuario Varchar(50))
As
Begin
      Insert Into Tbl_Departamento(Departamento,UsuarioReg)
	   Values (@Departamento,@Usuario)
	  RaisError('Departamento registrado correctamente',16,1)
End
Go
Create Table Tbl_Supervisor
(
IdSupervisor Int Not Null Identity(1,1) Primary key,
Supervisor Varchar(50) Not Null,
IdDepartamento Int Not Null,
Estado Char(1) Not Null Default('A'),
FecRegistro DateTime Not Null Default(Getdate()),
UsuarioReg Varchar(50) Not Null
)
Go
Create Procedure Pr_RegistroSupervisor(@Supervisor Varchar(50),@Dpt Varchar(50),@Usuario Varchar(50))
As
Begin
Declare @IdDepartamento Int
Set @IdDepartamento = (Select IdDepartamento From Tbl_Departamento Where Departamento = @dpt)
Insert Into Tbl_Supervisor(Supervisor,IdDepartamento,UsuarioReg)
Values(@Supervisor,@IdDepartamento,@Usuario) 
End
Go
CREATE TABLE [dbo].[Servicios](
	[IdServicio] [int] IDENTITY(1,1) NOT NULL,
	[Servicios] [varchar](150) NOT NULL,
	[Estado] [char](1) NOT NULL,
	[FecRegistro] [datetime] NOT NULL,
	[UsuarioReg] [varchar](80) NOT NULL,
	[Comision] [decimal](16, 2) NULL,
	[Compania] [varchar](150) NULL,
PRIMARY KEY CLUSTERED 
(
	[IdServicio] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
GO

ALTER TABLE [dbo].[Servicios] ADD  DEFAULT ('A') FOR [Estado]
GO

ALTER TABLE [dbo].[Servicios] ADD  DEFAULT (getdate()) FOR [FecRegistro]
GO

ALTER TABLE [dbo].[Servicios] ADD  DEFAULT ((0.00)) FOR [Comision]
GO
Create Procedure [dbo].[Pr_Servicios]
As
Select ROW_NUMBER() OVER(ORDER BY IdServicio ASC) AS Id, Servicios From Servicios Where Compania = 'DiazSoft Tecnology SRL' And Estado = 'A'
GO
Create Table Tbl_AvanceEfectivo
(
IdAvance Int Not Null Identity(1,1),
EmpleadoDocumento Varchar(25) Not Null,
MontoSolicitado Decimal(16,2) Not Null,
Cuotas Int Not Null,
Obs Varchar(200) Null,
Estado Char(1) Not Null Default('A'),
FecRegistro DateTime Not Null Default(Getdate()),
UsuarioReg Varchar(50) Not Null
)
Go
Create Procedure Pr_RegistroAvanceEfectivo(@Empleado Varchar(25),@Monto Decimal(16,2),@cuotas Int,@Obs Varchar(200),@Usuario Varchar(50))
As
Begin 
Insert Into Tbl_AvanceEfectivo(EmpleadoDocumento,MontoSolicitado,Cuotas,Obs,UsuarioReg)
  Values (@Empleado,@Monto,@cuotas,@Obs,@Usuario)
End
Go
Create Procedure Pr_ConsultaSupervisor
As
Select IdSupervisor, Supervisor, IdDepartamento From Tbl_Supervisor Where Estado = 'A'
Go
Create Procedure Pr_Conteorrhh 
As
Begin
Declare @Empleados Decimal(16,2)
Declare @Puestos Varchar(5)
Declare @Departamento Varchar(5)
Declare @Sueldos Decimal(16,2)

Set @Empleados = (Select Sum(MontoSolicitado)Monto From Tbl_AvanceEfectivo Where Estado = 'A')
Set @Puestos = (Select Count(*) From Tbl_Cargos Where Estado = 'A')
Set @Departamento = (Select Count(*) From Tbl_Departamento Where Estado = 'A')
Set @Sueldos = (Select IsNUll(Sum(Sueldo),0.00) From Tbl_Empleados Where Estado = 'A')

Select CONVERT(varchar, convert(money, @Empleados), 1) Empleados, @Puestos Puestos, @Departamento Dpt, CONVERT(varchar, convert(money, @Sueldos), 1)Sueldo
End
Go
Create Table Tbl_AvanceSaldo
(
IdSaldo Int Not Null Identity(1,1),
IdAvance Int Not Null,
NroCuota Int Not Null,
MontoSaldo Decimal(16,2) Not Null,
FecRegistro DateTime Not Null Default(Getdate()),
UsuarioReg Varchar(50) Not Null
)
Go
Create Procedure Pr_ConsultaDeuda
As
Begin
Select A.IdAvance, Concat(E.Nombre,' ',E.Apellido)Colaborador, CONVERT(varchar, convert(Money, MontoSolicitado), 1)MontoSolicitado, 
       CONVERT(varchar, convert(money, IsNull(MontoSaldo,0)), 1)MontoSaldo, Cuotas, (Cuotas - (isNUll(NroCuota,0))) Cuota_Restante
From   Tbl_AvanceEfectivo A Left Outer Join Tbl_AvanceSaldo S On A.IdAvance = S.IdAvance
       Inner Join Tbl_Empleados E On E.NumDoc = A.EmpleadoDocumento 
End
Go
Create Table Tbl_TSS
(
IdTss Int Not Null Identity(1,1),
Descripcion Varchar(100) Not Null,
Abreviatura Varchar(25) Not NUll,
PorcientoAporte Decimal(16,2) Not Null,
FecRegistro DateTime Not Null Default(Getdate()),
UsuarioReg Varchar(50) Not Null,
FecMod DateTime Null,
UsuarioMod Varchar(50) Null
)
Go
Insert Into Tbl_TSS (Descripcion,Abreviatura,PorcientoAporte,UsuarioReg)
Values ('Seguro Familiar de Salud','SFS',3.04,'System'),('Administradora de Fondos de Pensiones','AFP',2.87,'System')
Go
Create Table Tbl_ISR
(
Escala Int Not Null Identity(1,1),
EscalaMonto1 Decimal(16,2) Not Null,
EscalaMonto2 Decimal(16,2) Not Null,
Aporte Int Not Null,
FecRegistro DateTime Not Null Default(Getdate()),
UsuarioReg Varchar(50) Not Null,
FecMod DateTime Null,
UsuarioMod Varchar(50) Null
)
Go
Insert Into Tbl_ISR (EscalaMonto1,EscalaMonto2,Aporte,UsuarioReg)
Values (0,399923.00,0,'System'),(399923.01,599884.00,15,'System'),(599884.01,833171.00,20,'System'),(833171.01,1000000000,25,'System')
Go
Create Procedure Pr_TSS
As
Select IdTss Id, Abreviatura Concepto, PorcientoAporte From Tbl_TSS
Go
Create Procedure Pr_ISR
As
Select Escala, CONVERT(varchar, convert(Money, EscalaMonto1), 1) Desde, CONVERT(varchar, convert(Money, EscalaMonto2), 1) Hasta, Concat(Aporte,' ','%')Aporte From Tbl_ISR
Go
Declare @SFS Float = (Select CAST(PorcientoAporte AS float)/100 From Tbl_TSS Where Abreviatura = 'SFS')
Declare @AFP Float = (Select CAST(PorcientoAporte AS float)/100 From Tbl_TSS Where Abreviatura = 'AFP')
Select IdEmpleado, Concat(Nombre,' ',Apellido)Colaborador, Sueldo, (Sueldo*@SFS)SFS, (Sueldo*@AFP)AFP
From Tbl_Empleados --Where IdEmpleado = 1
Go
Create Table Tbl_TiposDescuento
(
IdTipo Int Not Null Identity(1,1),
TipoDescuento Varchar(50) Not Null,
Estado Char(1) Not Null Default('A'),
FecRegistro DateTime Not Null Default(Getdate()),
UsuarioReg Varchar(50) Not Null,
FecMod DateTime Null,
UsuarioMod Varchar(50) Null
)
Go
Insert Into Tbl_TiposDescuento (TipoDescuento,UsuarioReg)
Values ('PriceSmart','System'),('Gimnasio','System'),('Farmacia','System'),('Almuerzo','System'),('Transporte','System'),('Viaje','System')
Go
Create Table Tbl_OtrosDescuentos
(
IdDescuento Int Not Null Identity(1,1),
Tipo Varchar(25) Not Null,
IdEmpleado Int Not Null,
MontoDescuento Decimal(16,2) Not Null,
Estado Char(1) Not Null Default('A'),
FecRegistro DateTime Not Null Default(Getdate()),
UsuarioReg Varchar(50) Not Null
)
Go
Create Procedure Pr_RegistroDescuento (@IdTipo Varchar(25),@Empleado Varchar(25),@Monto Decimal(16,2), @Usuario Varchar(25))
As
Insert Into Tbl_OtrosDescuentos (Tipo, IdEmpleado, MontoDescuento, UsuarioReg)
Values (@IdTipo,@Empleado,@Monto,@Usuario)
Go
Create Table Tbl_SaldoDescuentos
(
IdSaldoDescuento Int Not Null Identity(1,1),
IdDescuento Int Not Null,
MontoSaldo Decimal(16,2) Not Null,
IdNomina Int Not Null,
FecRegistro DateTime Not Null Default(Getdate()),
UsuarioReg Varchar(50) Not Null
)
Go
Create Procedure Pr_TiposDescuento
As
Select IdTipo, TipoDescuento, UsuarioReg From Tbl_TiposDescuento Where Estado = 'A'
Go
Select OD.IdDescuento Id, Tipo, MontoDescuento, Sum(MontoSaldo)Saldado, Concat(E.Nombre,' ',E.Apellido)Colaborador 
From   Tbl_OtrosDescuentos OD
       Left Outer Join Tbl_SaldoDescuentos SD On OD.IdDescuento = SD.IdDescuento And OD.Estado = 'A'
	   Inner Join Tbl_Empleados E On E.IdEmpleado = OD.IdEmpleado
Group By OD.IdDescuento, Tipo, MontoDescuento, Concat(E.Nombre,' ',E.Apellido)
Go
CREATE TABLE [dbo].[Entidad](
	[IdEntidad] [int] IDENTITY(1,1) NOT NULL,
	[Nombre] [varchar](100) NULL,
	[Apellido] [varchar](100) NULL,
	[RazonSocial] [varchar](200) NOT NULL,
	[NroTelefono] [varchar](50) NULL,
	[TipoTelefono] [varchar](50) NULL,
	[Nrotelefono1] [varchar](50) NULL,
	[Tipotelefono1] [varchar](50) NULL,
	[TipoEnte] [varchar](70) NOT NULL,
	[Sexo] [varchar](50) NOT NULL,
	[NroDoc] [varchar](50) NOT NULL,
	[TipoDoc] [varchar](50) NOT NULL,
	[TipoDomicilio] [varchar](50) NULL,
	[Calle] [varchar](150) NULL,
	[Sector] [varchar](100) NULL,
	[Departamento] [varchar](100) NULL,
	[Pais] [varchar](150) NULL,
	[WebSite] [varchar](150) NULL,
	[Email] [varchar](100) NULL,
	[Empresa] [varchar](150) NULL,
	[Cargo] [varchar](100) NULL,
	[GiroNegocio] [varchar](150) NULL,
	[Cortesia] [varchar](10) NULL,
	[EstadoCivil] [varchar](50) NULL,
	[FecAniversario] [datetime] NULL,
	[Instagram] [varchar](60) NULL,
	[FaceBook] [varchar](60) NULL,
	[Estado] [char](1) NULL,
	[FecRegistro] [datetime] NOT NULL,
	[UsuarioReg] [varchar](100) NOT NULL,
	[Compania] [varchar](150) NULL,
PRIMARY KEY CLUSTERED 
(
	[IdEntidad] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON)
)
GO
ALTER TABLE [dbo].[Entidad] ADD  DEFAULT ('A') FOR [Estado]
GO
ALTER TABLE [dbo].[Entidad] ADD  DEFAULT (getdate()) FOR [FecRegistro]
GO
CREATE Procedure [dbo].[Pr_ConsultaEntidad]
As
Select IdEntidad,RazonSocial,TipoEnte,Sexo,TipoDoc,NroDoc,FecRegistro,UsuarioReg,isNull(NroTelefono,'No Indicado')Telefono
From Entidad 
Where Compania = 'DiazSoft Tecnology SRL'
Go
